import secrets

from flask import (
    Flask,
    escape,
    request,
    flash,
    url_for,
    render_template,
    Response,
    abort,
    redirect,
    jsonify,
    session,
)

from flask import Blueprint
import random


# from app.forms import InitForm, RunForm
# from app.utils import manage_session

from src import logger
from src.pgn import PGN

# Blue prints
home = Blueprint("home", __name__)


# static files
@home.route("/", methods=["GET"])
def just_static():
    """just return html and css"""

    logger.warning("called")
    # manage_session("/")
    # initForm = InitForm(request.form)
    # runForm = RunForm(request.form)

    # make pgn and data manipulation
    pgn = PGN("black").as_dict()

    return render_template("pages/home.html", pgn=pgn)
