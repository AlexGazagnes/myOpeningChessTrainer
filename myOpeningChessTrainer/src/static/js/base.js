
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function changeBeforeImage() {

    await sleep(1000);

    let n = 0;
    while (n < 100) {

        await sleep(1000);

        document.getElementById("lastBeforeImg").style.display = "none";
        document.getElementById("beforeImg").style.display = "inline";

        await sleep(8000);

        document.getElementById("lastBeforeImg").style.display = "inline";
        document.getElementById("beforeImg").style.display = "none";

        n++;
    }
}

async function changeAfterImage(id) {

    let n = 0;
    while (n < 100) {

        await sleep(8000);

        document.getElementById("afterImg").style.display = "inline";
        document.getElementById(id).style.display = "none";

        await sleep(1500);

        document.getElementById("afterImg").style.display = "none";
        document.getElementById(id).style.display = "inline";

        n++;
    }
}


function moove_choice(key, good, score, nature, comment) {

    // display
    $("#info").css("display", "inline");
    $("#good").css("display", "inline");
    $("#infoComp").css("display", "none");
    $("#reload").css("display", "none");

    // html ans
    if (good > 0) {
        ans = 'GOOD';
        $("#infoComp").css("display", "inline");
        $("#infoComp").html("score is " + score + ", nature is " + nature + ", comment is " + comment);
        $("#reload").css("display", "inline");

    } else {
        ans = 'WRONG';
    }
    $("#good").html(ans);

    // image update
    if (good > 0) {
        $("#before").css("display", "none");
        $("#after").css("display", "inline");
        var name = '#' + key + "Img"
        console.log(name)
        $(name).css("display", "inline");
        changeAfterImage(key + "Img");
    }


}


changeBeforeImage();
