import io
import random

import chess
import chess.pgn
import chess.svg


from src import logger
from src.data import black_df as black


class PGN:
    """just pgn object """

    def __init__(self, color, depth=0, familly="", pgn=""):
        """init method """

        # color
        self.color = color
        if "b" in color.lower():
            df = black
            self.color == "black"
        elif "w" in color.lower():
            self.color == "white"
            raise NotImplementedError("NotImplementedError")

        # csv_ok and img_ok
        df = df.loc[df.csv_ok > 0.99, :]

        # depth
        if depth:
            df = df.loc[df.depth > 1, :]
            df = df.loc[df.depth == depth, :]

        # familly
        if familly:
            df = df.loc[df.familly == familly, :]

        # nothing found
        if not len(df):
            raise ArithmeticError("dataframe is empty")

        # line
        line = df.fillna("").sample(1).iloc[0].to_dict()

        # moove cands
        mooves_cands = ["moove_" + str(i) for i in (1, 2, 3)]
        # logger.warning(f"mooves_cands {mooves_cands} ")
        mooves = list()
        for k in line.keys():
            if any([m for m in mooves_cands if m in k]):
                mooves.append(k)

        # pgn attr exclude mooves
        for k, v in line.items():
            if not k in mooves:
                setattr(self, k, v)

        # moove_dict
        m_dict = {}
        for moove in mooves_cands:  # good moove
            if not moove in line.keys():
                continue
            if len(str(line[moove])) > 1:
                dd = {
                    k.replace(moove + "_", ""): v
                    for k, v in line.items()
                    if ((moove in k) and (moove != k))
                }
                # good
                dd["good"] = 1
                # pgn of the moove
                if "b" in self.color:
                    dd["pgn"] = f"{self.pgn}_{line[moove]}"
                else:
                    dd["pgn"] = f"{self.pgn}_{self.depth+1}._{line[moove]}"
                # update dict
                m_dict[line[moove]] = dd

        bad_mooves = line["moove_cands"].split(",")  # bad moove
        for moove in bad_mooves:
            if moove not in m_dict.keys():
                m_dict[moove] = {
                    "good": 0,
                    "score": -1,
                    "nature": "??",
                    "comment": "",
                    "pgn": "",
                }

        # shuffle :
        m_dict = [(i, j) for i, j in m_dict.items()]
        random.shuffle(m_dict)
        m_dict = {i: j for i, j in m_dict}
        # set attr
        logger.warning(f"m_dict, {m_dict} ")
        setattr(self, "mooves", m_dict)

        # # pgn and last_pgn
        # if self.color == "black":
        #     pgn = line["pgn"]
        #     if (self.depth - 1) == 0:
        #         last_pgn = "0"
        #     else:
        #         last_pgn = pgn.split("_")[:-1]
        #         if "." in last_pgn[-1]:
        #             last_pgn = last_pgn[:-1]
        #         last_pgn = "_".join(last_pgn)
        #         self.last_pgn = last_pgn

        # fen and svg and uci
        svg_color = chess.WHITE if "w" in self.color else chess.BLACK
        io_string = io.StringIO(self.pgn.replace("_", " "))
        game = chess.pgn.read_game(io_string)
        board = game.board()
        self.uci_mooves = []
        self.fen = [board.fen()]
        self.svg = [chess.svg.board(board, orientation=svg_color, size=350)]

        for i, move in enumerate(game.mainline_moves()):
            board.push(move)
            self.uci_mooves.append(str(move))
            self.fen.append(board.fen())
            self.svg.append(chess.svg.board(board, orientation=svg_color, size=350))

        # same for good_mooves :
        for moove in m_dict.keys():
            if not int(m_dict[moove]["good"]):
                continue
            io_str = io.StringIO(m_dict[moove]["pgn"].replace("_", " "))
            game = chess.pgn.read_game(io_str)
            board = game.board()
            # last_moove = game.mainline_moves()[-1]
            # mooves_except_last = game.mainline_moves()[:-1]
            for move in game.mainline_moves():
                board.push(move)
                m_dict[moove]["fen"] = board.fen()
                m_dict[moove]["svg"] = chess.svg.board(
                    board, orientation=svg_color, lastmove=move, size=350
                )
                m_dict[moove]["uci_moove"] = str(move)

        # str pgn
        self.pgn_str = self.pgn.replace("_", " ")

    def as_dict(self):
        """to json """

        return self.__dict__


if __name__ == "__main__":

    pgn = PGN("black")
    p = pgn.pgn_str()

    io_string = io.StringIO(pgn.pgn_str())
    game = chess.pgn.read_game(io_string)