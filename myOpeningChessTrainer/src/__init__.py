import os
import logging
import secrets

from flask import Flask, session
from flask_session import Session

# from flask.ext.session import Session
from flask import render_template, redirect, escape, request, session

# from flask_login import LoginManager
from flask_bcrypt import Bcrypt


# from flask_scss import Scss


from params import GeneralParams as Params
from params import setBasicConfig


# pre set the logger
setBasicConfig(os.getenv("SERVICE_NAME", "ipython"), Params)
logger = logging.getLogger()


# from src import *

# from app.config import ProdConfig, DevConfig

# replace by redis in next feature


sess = Session()
bcrypt = Bcrypt()


def create_app():
    """make app """

    # logger.debug("called")

    app = Flask(__name__)

    # Application Configuration
    # app.config.from_object(DevConfig)

    # plugin
    sess.init_app(app)

    # bcrypt.init_app(app)

    # reload jinja
    app.jinja_env.auto_reload = True
    app.config["TEMPLATES_AUTO_RELOAD"] = True

    # context manager
    with app.app_context():
        from src.home import home

        app.register_blueprint(home)
        return app