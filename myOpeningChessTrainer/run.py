# !/usr/bin/env python3
# coding: utf-8

import sys
import time

from params import GeneralParams as Params
from src import create_app

print(Params.__dict__)

# if called by gunicorn
app = create_app()


def main():
    """main funct ie if called by python run.py"""

    # create app
    app = create_app()
    # run app
    app.run(port=Params.port, host=Params.host)


if __name__ == "__main__":
    main()
